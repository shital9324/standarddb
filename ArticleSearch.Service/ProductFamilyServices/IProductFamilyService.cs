﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ProductFamilyServices
{
  public  interface IProductFamilyService
    {
        int InsertFamilies(ProductFamily productFamily);
        IEnumerable<ProductFamily> GetAll();
    }
}

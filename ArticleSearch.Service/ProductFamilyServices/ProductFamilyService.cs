﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArticleSearch.Data;
using System.Threading.Tasks;
using ArticleSearch.Repositories;




namespace ArticleSearch.Service.ProductFamilyServices
{
  public  class ProductFamilyService:IProductFamilyService
    {
      private IUnitOfWork _unitOfWork;
      public ProductFamilyService(IUnitOfWork unitOfWork)
      {
          _unitOfWork = unitOfWork;
      }

     public int InsertFamilies(ProductFamily productFamily )
    {
        int FamilyId = 0;
        if (string.IsNullOrWhiteSpace(productFamily.FamilyName) || string.IsNullOrWhiteSpace(productFamily.FamilyNumber))
        {
            return FamilyId;
        }
        if (productFamily != null)
        {
            var data = _unitOfWork.GetProductFamilyRepository().Get(s => s.FamilyNumber.Equals(productFamily.FamilyNumber)).FirstOrDefault();
            if (data != null) //allready exist
            {
                FamilyId = data.Id;
            }
            else
            {
                _unitOfWork.GetProductFamilyRepository().Insert(productFamily);
                _unitOfWork.Save();
                FamilyId =  productFamily.Id;
            }

        }
        else //if empty
        {
            FamilyId = 0;
        }
        return FamilyId;
    }

     public IEnumerable<ProductFamily> GetAll()
     {
         return _unitOfWork.GetProductFamilyRepository().Get();
     }

    }
}

﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.DimentionServices
{
    public interface IDimentionService
    {
        int InsertDimention(Dimension dimention);
         IEnumerable<Dimension> GetAll();
    }
}

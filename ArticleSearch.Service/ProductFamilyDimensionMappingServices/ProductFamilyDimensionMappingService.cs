﻿using ArticleSearch.Data;
using ArticleSearch.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ProductFamilyDimensionMappingServices
{
    public class ProductFamilyDimensionMappingService : IProductFamilyDimensionMappingService
    {
       private IUnitOfWork _unitOfWork;
       public ProductFamilyDimensionMappingService(IUnitOfWork unitOfWork)
      {
          _unitOfWork = unitOfWork;
      }

       public bool InsertProductFamilyDimensionMapping(ProductFamilyDimensionMapping productFamilyDimension)
       {
           bool flag;
           if (productFamilyDimension != null)
           {
               //all ready exist or not is pending temprary.
               //var data = _unitOfWork.GetDimensionRepository().Get(s => s.FamilyId.Equals(dimention.FamilyId)).Count();
               //if (data > 0) //allready exist
               //{
               //    flag = false;
               //}
               //else
               //{
               //    _unitOfWork.GetDimensionRepository().Insert(dimention);
               //    _unitOfWork.Save();
               //    flag = true;
               //}
               _unitOfWork.GetProductFamilyDimensionMappingRepository().Insert(productFamilyDimension);
               _unitOfWork.Save();
               flag = true;

           }
           else //if empty
           {
               flag = false;
           }
           return flag;
       }

       public IEnumerable<ProductFamilyDimensionMapping> GetAll()
       {
           return _unitOfWork.GetProductFamilyDimensionMappingRepository().Get();
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.ImportData
{
    public interface IImportDataService
    {
        void ImportFamilyData(string path);
        bool ImportAricleData(string path);

    }
}

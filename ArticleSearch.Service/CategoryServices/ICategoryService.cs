﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.CategoryServices
{
   public interface  ICategoryService
    {
       bool InsertCategory(Category category);
    }
}

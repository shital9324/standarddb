﻿using ArticleSearch.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArticleSearch.Data;

namespace ArticleSearch.Service.SupplyerServices
{
   public class SupplyerService:ISupplyerService
    {
       private IUnitOfWork _unitOfWork;
       public SupplyerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

       public int InsertSupplyer(Supplier supplier)
       {
           int supplierId = 0;
           if (supplier != null)
           {
               var data = _unitOfWork.GetSupplierRepository().Get(s => s.Name.Equals(supplier.Name) && s.Designation.Equals(supplier.Designation) ).FirstOrDefault();
               if (data != null) //allready exist
               {
                   supplierId = data.Id;
               }
               else
               {
                   _unitOfWork.GetSupplierRepository().Insert(supplier);
                   _unitOfWork.Save();
                   supplierId = supplier.Id;
               }

           }
           else //if empty
           {
               supplierId = 0;
           }
           return supplierId;
       }
    }
}

﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArticleSearch.Service.SupplyerServices
{
   public interface ISupplyerService
    {
        int InsertSupplyer(Supplier supplier);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace ArticleSearch.Model
{
   public class ArticleSearchParameters
    {
        public string FamilyNo { get; set; }
        public string ArticleNo { get; set; }
        public string ArticleName { get; set; }
        public string TechnicalData { get; set; }
        public string AdditionalData { get; set; }
        public string CompanyCode { get; set; }
        public string SpecificComppanyDesignation { get; set; }
        public int SG { get; set; }
        public string SC { get; set; }
        public string supplyerName { get; set; }
        public string supplyerDesignation { get; set; }
        public int hits { get; set; }

    }
}

﻿using ArticleSearch.Service;
using ArticleSearch.Service.ImportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArticleSearch.Service.ArticleServices;
using ArticleSearch.Data;
using Models;
using ArticleSearch.Service.ProductFamilyServices;
using ArticleSearch.Service.ProductFamilyDimensionMappingServices;
using System.Xml.Linq;
using ArticleSearch.Model;
using System.Collections;
using ArticleSearch.Web.Filters;


namespace ArticleSearch.Web.Controllers
{
    //[SessionExpireFilterAttribute]
    public class HomeController : Controller
    {
        private readonly IImportDataService _importDataService;
        private readonly IArticleService _articleService;
        private readonly IProductFamilyService _productFamilyService;
        private readonly IProductFamilyDimensionMappingService _productFamilyDimensionMappingService;
        public HomeController(IImportDataService importDataService, IArticleService articleService, IProductFamilyService productFamilyService, IProductFamilyDimensionMappingService productFamilyDimensionMappingService)
        {
            _importDataService = importDataService;
            _articleService = articleService;
            _productFamilyService = productFamilyService;
            _productFamilyDimensionMappingService = productFamilyDimensionMappingService;
        }


        public ActionResult Index()
        {
           // if (Session["UserName"]==null)
           // {
           //     return RedirectToAction("Index", "Login");
           // }
        	//MyTest
             string family = "export_electrical_articles_families_.xlsx";
             string article = "export_electrical_articles.xlsx";
             string familyPath = Server.MapPath(string.Concat("\\Uploaded_Files\\", family));
             string articlePath = Server.MapPath(string.Concat("\\Uploaded_Files\\", article));
           //  _importDataService.ImportFamilyData(familyPath);
            // _importDataService.ImportAricleData(articlePath);
            return View();
        }

        public ActionResult Search_Article_or_Family(string number)
        {
            number = "1089 9284 01";
            if (number == string.Empty || number == "")
            {
                //error message
            }
            else
            {
                //search article
                var articleData = _articleService.GetAll().Where(x => x.ArticleNo.Equals(number)).Select(a => new ArticleDataField { Id = a.Id, ArticleNo = a.ArticleNo, ArticleName = a.ArticleName, ProductFamilyId = a.ProductFamilyId, ProductFamilyNo = a.ProductFamily.FamilyNumber, ProductFamilName = a.ProductFamily.FamilyName, DrawingNo = a.DrawingNo, SC = a.SC, SG = a.SG, SupplierName = a.Supplier.Name, SupplierDesignation = a.Supplier.Designation, UsedAt = a.UsedAt, ArticleXMLdata = a.ArticleDimensonData }).ToList();
                if (articleData.Count > 0)// !=null
                {
                    foreach (var item in articleData)
                    {
                        var xmlString = item.ArticleXMLdata;
                        XDocument xmlDoc = XDocument.Parse(xmlString);
                        List<string> dims = new List<string>(); //dims count is fixed
                        foreach (var cordinate in xmlDoc.Descendants("Dimension"))
                        {
                            dims.Add(cordinate.Attribute("value").Value);
                        }
                        item.ArticleDimensonData = dims;
                        item.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(item.ProductFamilyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family;
                        List<dynamic> lst = new List<dynamic>();
                        lst.Add(item);
                        ViewBag.articleData = lst;
                    }

                }
                else
                {
                    //article items not found
                    ViewBag.articleData = null;

                }



            }




            return View();
        }
        [HttpPost]
        public ActionResult Search_Article_by_parameter(ArticleSearchParameters searchParameters)
        {
            // searchParameters.ArticleNo = "1089 9284 01";
            //   searchParameters.FamilyNo = "0983 2460 00";
            // searchParameters.ArticleName = "CAPACITOR";
            //searchParameters.SG = 1;
            //searchParameters.SC = "1;1;2";
            searchParameters.TechnicalData = "± 20 %";// "10000 µF";

            List<ArticleDataField> FilteredArticles = new List<global::Models.ArticleDataField>();
            var All_articleData = _articleService.GetAll().Select(a => new ArticleDataField { Id = a.Id, ArticleNo = a.ArticleNo, ArticleName = a.ArticleName, ProductFamilyId = a.ProductFamilyId, ProductFamilyNo = a.ProductFamily.FamilyNumber, ProductFamilName = a.ProductFamily.FamilyName, DrawingNo = a.DrawingNo, SC = a.SC, SG = a.SG, SupplierName = a.Supplier.Name, SupplierDesignation = a.Supplier.Designation, UsedAt = a.UsedAt, ArticleXMLdata = a.ArticleDimensonData }).ToList();

            FilteredArticles = All_articleData;
            if (searchParameters.ArticleNo != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.ArticleNo.Equals(searchParameters.ArticleNo)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.FamilyNo != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.ProductFamilyNo.Equals(searchParameters.FamilyNo)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.ArticleName != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.ArticleName.Equals(searchParameters.ArticleName)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.SG != 0)
            {
                FilteredArticles = FilteredArticles.Where(x => x.SG.Equals(searchParameters.SG)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.SC != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.SC.Equals(searchParameters.SC)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.SpecificComppanyDesignation != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.SpecificCompany.Equals(searchParameters.SpecificComppanyDesignation)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            if (searchParameters.TechnicalData != null)
            {
                FilteredArticles = FilteredArticles.Where(x => x.ArticleXMLdata.Contains(searchParameters.TechnicalData)).ToList();
                if (FilteredArticles.Count == 0)
                {
                    ViewBag.Data = null;
                    return View();
                }
            }
            foreach (var item in FilteredArticles)
            {
                item.ArticleDimensonData = xmlToList(item.ArticleXMLdata);
                item.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(item.ProductFamilyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family; //familyDimColoms(item.ProductFamilyId);
            }
            ViewBag.Data = FilteredArticles;
            return View();
        }

        public List<string> xmlToList(dynamic xmlString)
        {
            XDocument xmlDoc = XDocument.Parse(xmlString);
            List<string> dims = new List<string>(); //dims count is fixed
            foreach (var cordinate in xmlDoc.Descendants("Dimension"))
            {
                dims.Add(cordinate.Attribute("value").Value.Trim());
            }
            return dims;
        }
        public IEnumerable<string> familyDimColoms(int familyId)
        {
            var articleData = new ArticleDataField();
            articleData.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(familyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family;
            return articleData.ArticleDimensonColoms;

        }

    }

}

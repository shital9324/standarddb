﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArticleSearch.Web.Controllers
{
    public class MechanicalController : Controller
    {
        //
        // GET: /Mechanical/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Fasteners()
        {
            return View();
        }
        public ActionResult Pins()
        {
            return View();
        }

        public ActionResult Parallel_Pins()
        {
            return View();
        }
        public ActionResult Article_Info()
        {
            return View();
        }
        public ActionResult Family_List()
        {
            return View();
        }
        public ActionResult Family_Fig()
        {
            return View();
        }
        public ActionResult Search_Form()
        {
            return View();
        }
    }
}

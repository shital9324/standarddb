﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArticleSearch.Data;
using ArticleSearch.Service.UserServices;
using ArticleSearch.Web.Filters;
using ArticleSearch.Web.Models;

namespace ArticleSearch.Web.Controllers
{
    //git commit test: my test
    public class LoginController : Controller
    {
        private readonly IUserService _userService;
  
        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        
        [HttpPost]
        public ActionResult Index(UserLogIn user)
        {
            if (string.IsNullOrWhiteSpace(user.UserName) || string.IsNullOrWhiteSpace(user.Password))
            {
                ViewBag.errorMsg = "Please Enter Username and Password.";
                return View();
            }
            //check for valid user
            var currentUser = _userService.GetUserByUserNameAndPassword(user.UserName, user.Password);
            if (currentUser == null)
            {
                ViewBag.errorMsg = "Incorrect Username Or Password.";
                return View();
            }
            Session["UserId"] = currentUser.Id;
            Session["UserName"] = currentUser.UserName;
            Session["UserRole"] = currentUser.UserRole.Role;

            //reidrect base on user Role
            if (Session["UserRole"].ToString() == "Admin")
            {
                //admin view
                ViewBag.errorMsg = "Admin Panel comming soon";
                return View();
                
            }
            else
            {
                //employee view
                return RedirectToAction("Index", "Home");
            }
            
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            //Session["UserId"] = null;
            //Session["UserName"] = null;
            //Session["UserRole"] = null;
            return RedirectToAction("Index");
        }


        


    }
}

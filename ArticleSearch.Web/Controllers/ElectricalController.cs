﻿using ArticleSearch.Service;
using ArticleSearch.Service.ImportData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ArticleSearch.Service.ArticleServices;
using ArticleSearch.Data;
using Models;
using ArticleSearch.Service.ProductFamilyServices;
using ArticleSearch.Service.ProductFamilyDimensionMappingServices;
using ArticleSearch.Service.DimentionServices;

using System.Xml.Linq;
using ArticleSearch.Model;
using System.Collections;

using ArticleSearch.Web.Models;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Data;

namespace ArticleSearch.Web.Controllers
{
    public class ElectricalController : Controller
    {
        //
        // GET: /Electrical/
        #region Decl
        private readonly IImportDataService _importDataService;
        private readonly IArticleService _articleService;
        private readonly IProductFamilyService _productFamilyService;
        private readonly IProductFamilyDimensionMappingService _productFamilyDimensionMappingService;
        private readonly IDimentionService _dimentionService;
        #endregion

        #region Ctro
        public ElectricalController(IImportDataService importDataService, IArticleService articleService, IProductFamilyService productFamilyService, IProductFamilyDimensionMappingService productFamilyDimensionMappingService, IDimentionService dimentionService)
        {
            _importDataService = importDataService;
            _articleService = articleService;
            _productFamilyService = productFamilyService;
            _productFamilyDimensionMappingService = productFamilyDimensionMappingService;
            _dimentionService = dimentionService;
        }

#endregion

        #region method
        public ActionResult Index()
        {
            return View();
            //test
        }
        public ActionResult Rotating()
        {
            return View();
        }
        public ActionResult Asynchronous()
        {
            return View();
        }
        public ActionResult Slip_ring()
        {
            return View();
        }
        public ActionResult Article_Info()
        {
            return View();
        }
        public ActionResult Family_List()
        {
            
            return View();
        }
        public ActionResult Family_Fig()
        {
            return View();
        }
        public ActionResult Search_Form()
        {
            return View();
        }


       
        [HttpPost]
        public JsonResult SearchForm_byParameters(ArticleSearchParameters searchParameters)
        {
            //ArticleSearchParameters searchParameters = new ArticleSearchParameters();
            //searchParameters.ArticleNo = articleNo; //"1089 9284 01";
            //searchParameters.FamilyNo = familyNo;  //"0983 2460 00";
            //searchParameters.ArticleName = articleName; //"CAPACITOR";
            //searchParameters.SG = SG; //1;
            //searchParameters.SC = SC;  //"1;1;2";
            //searchParameters.TechnicalData = TechnicalData; //"± 20 %";// "10000 µF";
            //searchParameters.supplyerName = supplyerName;//"";
            //searchParameters.supplyerDesignation = supplyerDesignation; //"";

            //test
            List<ArticleMetadata> FilteredArticles = (from p in _articleService.GetAll()
                                   group p by p.ProductFamily.FamilyNumber into g
                                    select new ArticleMetadata
                                    {
                                        
                                        ProductFamilyNo = g.Key,

                                        ArticleData = g.ToList().Select(a => new ArticleData
                                        {
                                            Id = a.Id,
                                            ArticleNo = a.ArticleNo,
                                            ArticleName = a.ArticleName,
                                            ProductFamilyId = a.ProductFamilyId,
                                            ProductFamilyNo = a.ProductFamily.FamilyNumber,
                                            ProductFamilName = a.ProductFamily.FamilyName,
                                            DrawingNo = a.DrawingNo,
                                            SC = a.SC,
                                            SG = a.SG,
                                            SupplierName = a.Supplier.Name,
                                            SupplierDesignation = a.Supplier.Designation,
                                            UsedAt = a.UsedAt,
                                            ArticleXMLdata = a.ArticleDimensonData                                        
                                        }).ToList()
                                    }).ToList();

             

            //ends

            //List<ArticleData> FilteredArticles = new List<global::Models.ArticleData>();
            //var All_articleData = _articleService.GetAll().Select(a => new ArticleData
            //{
            //    Id = a.Id,
            //    ArticleNo = a.ArticleNo,
            //    ArticleName = a.ArticleName,
            //    ProductFamilyId = a.ProductFamilyId,
            //    ProductFamilyNo = a.ProductFamily.FamilyNumber,
            //    ProductFamilName = a.ProductFamily.FamilyName,
            //    DrawingNo = a.DrawingNo,
            //    SC = a.SC,
            //    SG = a.SG,
            //    SupplierName = a.Supplier.Name,
            //    SupplierDesignation = a.Supplier.Designation,
            //    UsedAt = a.UsedAt,
            //    ArticleXMLdata = a.ArticleDimensonData
            //}).ToList();

            //FilteredArticles = All_articleData;
//serverside filter commented here
            //if (searchParameters.ArticleNo != "")
            //{
                
            //    FilteredArticles = FilteredArticles.Where(x => x.ArticleData[0].ArticleNo.Equals(searchParameters.ArticleNo)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
               
            //}
            //if (searchParameters.FamilyNo != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.ProductFamilyNo.Equals(searchParameters.FamilyNo)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.ArticleName != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.ArticleData[0].ArticleName.Equals(searchParameters.ArticleName)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.SG != 0)
            //{
            //    FilteredArticles = FilteredArticles.Where(x =>x.SG.Equals(searchParameters.SG)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.SC != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.SC.Equals(searchParameters.SC)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.SpecificComppanyDesignation != "" && searchParameters.SpecificComppanyDesignation != null)
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.SpecificCompany.Equals(searchParameters.SpecificComppanyDesignation)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.TechnicalData != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.ArticleXMLdata.Contains(searchParameters.TechnicalData)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.supplyerName != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.SupplierName.Contains(searchParameters.supplyerName)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //if (searchParameters.supplyerDesignation != "")
            //{
            //    FilteredArticles = FilteredArticles.Where(x => x.SupplierDesignation.Contains(searchParameters.supplyerDesignation)).ToList();
            //    if (FilteredArticles.Count == 0)
            //    {
            //        ViewBag.Data = null;
            //        return null;//View();
            //    }
            //}
            //int i = 0;
            try
            {
                foreach (var item in FilteredArticles)
                {
                    for (int i = 0; i < item.ArticleData.Count; i++)
                    {
                        item.ArticleData[i].ArticleDimensonData = xmlToList(item.ArticleData[i].ArticleXMLdata);
                        item.ArticleData[i].ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(item.ArticleData[0].ProductFamilyId)).Select(a => a.Dimension.Name).Distinct();  //get all meta data cols of current family; //familyDimColoms(item.ProductFamilyId);    
                    }

                    // item.ArticleDimensonColoms = item.ArticleDimensonColoms.Select(s => s.Trim());

                }

            }
            catch (Exception)
            {
                
                
            }
           
            //ViewBag.Data = FilteredArticles;
            return Json(FilteredArticles, JsonRequestBehavior.AllowGet); //View();
        }

        //public JsonResult SearchForm_byParameters_(string familyNo, string articleNo, string articleName, int SG, string SC, string TechnicalData, string supplyerName, string supplyerDesignation)
        //{
        //    ArticleSearchParameters searchParameters = new ArticleSearchParameters();
        //    searchParameters.ArticleNo = articleNo; //"1089 9284 01";
        //    searchParameters.FamilyNo = familyNo;  //"0983 2460 00";
        //    searchParameters.ArticleName = articleName; //"CAPACITOR";
        //    searchParameters.SG = SG; //1;
        //    searchParameters.SC = SC;  //"1;1;2";
        //    searchParameters.TechnicalData = TechnicalData; //"± 20 %";// "10000 µF";
        //    searchParameters.supplyerName = supplyerName;//"";
        //    searchParameters.supplyerDesignation = supplyerDesignation; //"";

        //    List<_productFamily> FilteredArticles = new List<_productFamily>(); //new List<global::Models.ArticleData>();

        //    //var aaAll_articleData = (from pf in _productFamilyService.GetAll()
        //    //                         where pf.FamilyNumber == "0983 2460 00"
        //    //                         select pf);


        //    var familyList = (from pf in _productFamilyService.GetAll()
        //                      select pf.Articles. ).ToList();
        //    //var familyList_ =_productFamilyService.GetAll().Select(a=> new ArticleData_{ArticleDimensonColoms = a.Articles.ID, a.Articles. } )




        //    //FilteredArticles = All_articleData;
        //    //if (searchParameters.ArticleNo != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x =>x.Articles.  x.Articles ArticleNo.Equals(searchParameters.ArticleNo)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.FamilyNo != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.ProductFamilyNo.Equals(searchParameters.FamilyNo)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.ArticleName != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.ArticleName.Equals(searchParameters.ArticleName)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.SG != 0)
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.SG.Equals(searchParameters.SG)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.SC != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.SC.Equals(searchParameters.SC)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.SpecificComppanyDesignation != "" && searchParameters.SpecificComppanyDesignation != null)
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.SpecificCompany.Equals(searchParameters.SpecificComppanyDesignation)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    if (searchParameters.TechnicalData != "")
        //    {

        //        foreach (var item in familyList)
        //        {
        //            var productfamily = new _productFamily()
        //            {
        //                Id = item.Id,
        //                FamilyNumber = item .FamilyNumber,
        //                FamilyName = item.FamilyName,
        //                StandardReference = item.StandardReference,
        //                CategoryId = item.CategoryId,
        //                Status = item.Status,
        //                CreatedBy = item.FamilyName,
        //                CreatedDate = item.CreatedDate,
        //                ProductFamilyDimensionMappings = item.ProductFamilyDimensionMappings,
        //                Articles = new List<ArticleData_>()
        //            };
        //            foreach (var productfamilyArticle in item.Articles)
        //                if (productfamilyArticle.ArticleDimensonData.Contains(searchParameters.TechnicalData))
        //                {
        //                    productfamilyArticle.ArticleDimensonData = xmlToList(productfamilyArticle.ArticleDimensonData);
        //                    productfamily.Articles.Add(productfamilyArticle);
        //                }

        //            FilteredArticles.Add(productfamily);
        //        }



        //        if (FilteredArticles.Count == 0)
        //        {
        //            ViewBag.Data = null;
        //            return null;//View();
        //        }


        //    }
        //    //if (searchParameters.supplyerName != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.SupplierName.Contains(searchParameters.supplyerName)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //if (searchParameters.supplyerDesignation != "")
        //    //{
        //    //    FilteredArticles = FilteredArticles.Where(x => x.SupplierDesignation.Contains(searchParameters.supplyerDesignation)).ToList();
        //    //    if (FilteredArticles.Count == 0)
        //    //    {
        //    //        ViewBag.Data = null;
        //    //        return null;//View();
        //    //    }
        //    //}
        //    //foreach (var item in FilteredArticles)
        //    //{
        //    //    item.ArticleDimensonData = xmlToList(item.ArticleXMLdata);
        //    //    item.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(item.ProductFamilyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family; //familyDimColoms(item.ProductFamilyId);
        //    //}
        //    ////ViewBag.Data = FilteredArticles;
        //    return Json(FilteredArticles, JsonRequestBehavior.AllowGet); //View();
        //}

        public JsonResult Search_Article_(string number)
        {
            number = "1089 9284 01";
            if (number == string.Empty || number == "")
            {
                //error message
            }
            else
            {
                //search article
                var articleData = _articleService.GetAll().Where(x => x.ArticleNo.Equals(number)).Select(a => new ArticleDataField { Id = a.Id, ArticleNo = a.ArticleNo, ArticleName = a.ArticleName, ProductFamilyId = a.ProductFamilyId, ProductFamilyNo = a.ProductFamily.FamilyNumber, ProductFamilName = a.ProductFamily.FamilyName, DrawingNo = a.DrawingNo, SC = a.SC, SG = a.SG, SupplierName = a.Supplier.Name, SupplierDesignation = a.Supplier.Designation, UsedAt = a.UsedAt, ArticleXMLdata = a.ArticleDimensonData }).ToList();
                if (articleData.Count > 0)// !=null
                {
                    foreach (var item in articleData)
                    {
                        var xmlString = item.ArticleXMLdata;
                        XDocument xmlDoc = XDocument.Parse(xmlString);
                        List<string> dims = new List<string>(); //dims count is fixed
                        foreach (var cordinate in xmlDoc.Descendants("Dimension"))
                        {
                            dims.Add(cordinate.Attribute("value").Value);
                        }
                        item.ArticleDimensonData = dims;
                        item.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(item.ProductFamilyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family;
                        List<dynamic> lst = new List<dynamic>();
                        lst.Add(item);
                        ViewBag.articleData = lst;
                    }

                }
                else
                {
                    //article items not found
                    ViewBag.articleData = null;

                }



            }




            return Json(ViewBag.articleData, JsonRequestBehavior.AllowGet);
        }

        public List<string> xmlToList(dynamic xmlString)
        {
            XDocument xmlDoc = XDocument.Parse(xmlString);
            List<string> dims = new List<string>(); //dims count is fixed
            foreach (var cordinate in xmlDoc.Descendants("Dimension"))
            {
                dims.Add(cordinate.Attribute("value").Value);
            }
            return dims;
        }
        public IEnumerable<string> familyDimColoms(int familyId)
        {
            var articleData = new ArticleDataField();
            articleData.ArticleDimensonColoms = _productFamilyDimensionMappingService.GetAll().Where(x => x.ProductFamilyId.Equals(familyId)).Select(a => a.Dimension.Name);  //get all meta data cols of current family;
            return articleData.ArticleDimensonColoms;

        }

        public ActionResult demo()
        {
            return View();
        }

        public JsonResult FamilyDataGrid([DataSourceRequest] DataSourceRequest request)
        {
            int catagoryid = 4;
            var familyListByCatagory = _productFamilyService.GetAll().Where(t => t.CategoryId.Equals(catagoryid));
            List<FamilyDataGrid> familyData = new List<FamilyDataGrid>();

            foreach (var item in familyListByCatagory)
            {
                FamilyDataGrid familyModel = new FamilyDataGrid()
                {
                    FamilyId = item.Id,
                    FamilyNumber = item.FamilyNumber,
                    FamilyName = item.FamilyName
                };
                familyData.Add(familyModel);
            }
            return Json(familyData.ToDataSourceResult(request) );
        }

        public JsonResult ArticleGroupedByFamily(int familyId, [DataSourceRequest] DataSourceRequest request)
        {
            List<ArticleGridModel> articles = new List<ArticleGridModel>();
            var artdata = _articleService.GetAll().Where(t => t.ProductFamilyId.Equals(familyId));
            var familyMetaDataList = new List<string>();
            var dimentionIds = _productFamilyDimensionMappingService.GetAll().Where(t => t.ProductFamilyId.Equals(familyId)).Select(t => t.DimensionId).ToList().Distinct();
            var Dimensions = _dimentionService.GetAll().Where(t => dimentionIds.Contains(t.Id)).ToList();

            foreach (var a in Dimensions)
            {
                familyMetaDataList.Add(a.Name);
            }
            ViewBag.ColumnSchema = familyMetaDataList;

            foreach (var item in artdata)
            {
                var art = new ArticleGridModel()
                {
                    ArticleNo = item.ArticleNo,
                    ArticleName = item.ArticleName
                };
                articles.Add(art);
            }

            return Json(articles.ToDataSourceResult(request));
        }
        #endregion
    }
}

﻿//using CoolerProduction.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArticleSearch.Web.Filters
{
    public class SessionExpireFilterAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            var UI=ctx.Session["UserName"];
            if (UI == null)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 401;
                    filterContext.HttpContext.Response.End();
                }

                // check if a new session id was generated
                var url = filterContext.HttpContext.Request.Url.PathAndQuery;
                url = url.Replace("&", "%26");
                filterContext.Result = new RedirectResult("~/Login/Index?returnUrl=" + url);
            }
            base.OnActionExecuting(filterContext);
        }
    }

    //public class AdminFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        HttpContext ctx = HttpContext.Current;
    //        var role =ctx.Session["Role"];
    //        bool isValid = true;
    //        if (role != null && !(role.ToString() == Constant.admin ||role.ToString() == Constant.superadmin))
    //            isValid = false;

    //        if (role==null || !isValid)
    //        {
    //            if (filterContext.HttpContext.Request.IsAjaxRequest())
    //            {
    //                filterContext.HttpContext.Response.StatusCode = 401;
    //                filterContext.HttpContext.Response.End();
    //            }

    //            // check if a new session id was generated
    //            var url = filterContext.HttpContext.Request.Url.PathAndQuery;
    //            url = url.Replace("&", "%26");
    //            filterContext.Result = new RedirectResult("~/Account/AccessDenied?returnUrl=" + url);
    //        }
    //        base.OnActionExecuting(filterContext);
    //    }
    //}

    //public class SuperAdminFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        HttpContext ctx = HttpContext.Current;
    //        var role = ctx.Session["Role"];
    //        bool isValid = true;
    //        if (role != null && role.ToString() != Constant.superadmin)
    //            isValid = false;

    //        if (role == null || !isValid)
    //        {
    //            if (filterContext.HttpContext.Request.IsAjaxRequest())
    //            {
    //                filterContext.HttpContext.Response.StatusCode = 401;
    //                filterContext.HttpContext.Response.End();
    //            }

    //            // check if a new session id was generated
    //            var url = filterContext.HttpContext.Request.Url.PathAndQuery;
    //            url = url.Replace("&", "%26");
    //            filterContext.Result = new RedirectResult("~/Account/AccessDenied?returnUrl=" + url);
    //        }
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
    //public class AdminAndTeamLeaderFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        HttpContext ctx = HttpContext.Current;
    //        var role = ctx.Session["Role"];
    //        bool isValid = true;
    //        if (role != null && !(role.ToString() == Constant.admin || role.ToString() == Constant.teamLeader || role.ToString() == Constant.superadmin))
    //            isValid = false;

    //        if (role == null || !isValid)
    //        {
    //            if (filterContext.HttpContext.Request.IsAjaxRequest())
    //            {
    //                filterContext.HttpContext.Response.StatusCode = 401;
    //                filterContext.HttpContext.Response.End();
    //            }

    //            // check if a new session id was generated
    //            var url = filterContext.HttpContext.Request.Url.PathAndQuery;
    //            url = url.Replace("&", "%26");
    //            filterContext.Result = new RedirectResult("~/Account/AccessDenied?returnUrl=" + url);
    //        }
    //        base.OnActionExecuting(filterContext);
    //    }
    //}
}
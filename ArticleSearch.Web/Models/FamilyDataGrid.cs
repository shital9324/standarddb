﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Models
{
    public class FamilyDataGrid
    {
        public int FamilyId { get; set; }
        
        public string FamilyNumber { get; set; }
       
        public string FamilyName { get; set; }
    }
}
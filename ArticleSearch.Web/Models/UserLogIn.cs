﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Models
{
    public class UserLogIn
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Nullable<int> UserRoleId { get; set; }

        public virtual UserRole UserRole { get; set; }
        //for return URL
        public string ReturnUrl { get; set; }
    }
}
﻿using ArticleSearch.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Models
{
    public class FamilyInfoModel
    {
        public int DepartmentId { get; set; }
        //family
        public int Id { get; set; }
        [Required]
        [Display(Name = "Family Number")]
        public string FamilyNumber { get; set; }
        [Required]
        [Display(Name = "Family Name")]
        public string FamilyName { get; set; }
        [Display(Name = "Standard Reference")]
        public string StandardReference { get; set; }
        [Required]
        [Display(Name = "Category Id")]
        public int CategoryId { get; set; }
        public Nullable<byte> Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public virtual Category Category { get; set; }
        //dimentions
        [Required]
        [Display(Name = "Dimension Name")]
        public List<string> DimName { get; set; }

    }
}
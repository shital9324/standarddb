﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticleSearch.Web.Models
{
    public class ArticleGridModel
    {
        public string ArticleNo { get; set; }
        public string ArticleName { get; set; }
        public int SG { get; set; }

        public string SC { get; set; }
        public string RespCompany { get; set; }
        public string DrawingNo { get; set; }
        public string ReplcaedBy { get; set; }

        public string FamilyNo { get; set; }
        public string BlankNo { get; set; }
        public string Material { get; set; }

        public string SurfaceTreatment { get; set; }
        public string UsedAt { get; set; }


        public List<string> FamilyMetaData { get; set; }
    }
}
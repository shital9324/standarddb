﻿using System.Web;
using System.Web.Optimization;

namespace ArticleSearch.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            
            //------------------------script bundles-------------------------------
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*", //it take all file start with unobtrusive name. e.g. jquery.unobtrusive-ajax, jquery.unobtrusive-ajax.min
                        "~/Scripts/jquery.validate*"));
            

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


  //js file of jqry and json
            bundles.Add(new ScriptBundle("~/Scripts").Include(
             "~/Scripts/jquery-2.1.4.js",
            
             "~/Scripts/jquery.unobtrusive*",
             "~/Scripts/jquery.validate*",
              "~/Scripts/json2.js" 
            ));

 //bootstrap js
            bundles.Add(new ScriptBundle("~/Content/bootstrap-3.3.4-dist/js").Include(
          "~/Content/bootstrap-3.3.4-dist/js/bootstrap-dialog.js" ,
          "~/Content/bootstrap-3.3.4-dist/js/bootstrap.min.js",
            "~/Content/bootstrap-3.3.4-dist/js/jquery.min.js"
          ));

 //kendo js
            bundles.Add(new ScriptBundle("~/Scripts/kendo/2016.1.412").Include(
           "~/Scripts/kendo/2016.1.412/jquery.min.js"
          ));
            bundles.Add(new ScriptBundle("~/Scripts/kendo").Include(
            "~/Scripts/kendo/kendo.all.min.js"
          ));




            //-----------------style bundles-------------------------------------------------

            bundles.Add(new StyleBundle("~/bundles/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

       //basic css with bootstrap(under content folder)
            bundles.Add(new StyleBundle("~/Content").Include(
                  "~/Content/master-style.css",
                  "~/Content/Site.css",
                  "~/Content/style.css"
                ));
            bundles.Add(new StyleBundle("~/Content/bootstrap-3.3.4-dist/css").Include( 
                "~/Content/bootstrap-3.3.4-dist/css/bootstrap.css",
                "~/Content/bootstrap-3.3.4-dist/css/bootstrap.min.css" //not comming
               ));

 //font
            bundles.Add(new StyleBundle("~/Content/font").Include(
               "~/Content/font/font-style.css"
              ));
            bundles.Add(new StyleBundle("~/Content/font-awesome-4.3.0/css").Include(
               "~/Content/font-awesome-4.3.0/css/font-awesome.css"
              ));
                  
 //                    //kendo css
            bundles.Add(new StyleBundle("~/Content/kendo/2016.1.412").Include(
              "~/Content/kendo/2016.1.412/kendo.common.min.css",
              "~/Content/kendo/2016.1.412/kendo.default.min.css",
               "~/Content/kendo/2016.1.412/kendo.default.mobile.min.css",
               "~/Content/kendo/2016.1.412/kendo.material.min.css"
             ));
    
                            
           
    

           
        }
    }
}
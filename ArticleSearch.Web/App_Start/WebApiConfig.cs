﻿using ArticleSearch.Repositories;
using ArticleSearch.Service;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Autofac.Integration.Mvc;
using System.Web.Mvc;
using ArticleSearch.Service.ImportData;
using ArticleSearch.Service.CategoryServices;
using ArticleSearch.Service.DimentionServices;
using ArticleSearch.Service.ProductFamilyDimensionMappingServices;
using ArticleSearch.Service.ProductFamilyServices;
using ArticleSearch.Service.SupplyerServices;
using ArticleSearch.Service.ArticleServices;
using ArticleSearch.Service.UserServices;


namespace ArticleSearch.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType(typeof(UnitOfWork)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ImportDataService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(CategoryService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ProductFamilyDimensionMappingService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ProductFamilyService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(DimentionService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(SupplyerService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(ArticleService)).AsImplementedInterfaces();
            builder.RegisterType(typeof(UserService)).AsImplementedInterfaces();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
}